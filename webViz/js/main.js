
/* Globar variable */
var hash;
var globalData;
var network = null;
var nodes, edges;
/* jmol configuration */
var Info;
;(function() {
	Info = {
		width: 500,
		height: 500,
		color: "#F3E5F5",
		addSelectionOptions: true,
		serverURL: "http://chemapps.stolaf.edu/jmol/jsmol/php/jsmol.php",
		use: "HTML5",
		readyFunction: null,
		defaultModel: "$", // PubChem -- use $ for NCI
	    bondWidth: 4,
	    label: true,
	    zoomScaling: 1.5,
	    pinchScaling: 2.0,
	    mouseDragFactor: 0.5,
	    touchDragFactor: 0.15,
	    multipleBondSpacing: 4,
	    spinRateX: 0.2,
	    spinRateY: 0.5,
	    spinFPS: 20,
	    spin:false,
	    debug: false
	}
})();

$(document).ready(function(){

	/* When the paths selector changes */
	$('#pathsList').change(function() {
    	var idx = this.selectedIndex;        
    	
    	if(idx > 0){
    		/* paint with list */
    		paint(globalData.paths[idx -1]);
    	}else{
    		/* just reset, don't paint path */
    		paint([]);
    	}
	});

	/* When the paths selector changes */
	$('#graphList').change(function() {
    	
    	var idx = this.selectedIndex;        
    	
    	if(idx > 0){
    		/* paint with list */
    		fetchGraph(this.value);
    	}
	});


	/* set JSMol sizes */
	$("#jmol_appletinfotablediv").css( "width" , "100%"); 
	$("#jmol_appletinfotablediv").css( "height" , "50%");

	/* load graph list */
	fetchGraphList("index.json");
});

function fetchGraphList(file){

	var url = 'data/' + file + "?val="+new Date().getTime();

	/* Load the list */
	$.getJSON( url, function(files) {

		/* sort alphabetically */
		files = files.sort(function(a,b){
			return a < b;
		});
		/* Populte dropdown with paths */
		$('#graphList').find('option').remove();
		$("#graphList").append($("<option />").text("none"));
		$.each(files, function() {
    		$("#graphList").append($("<option />").text(this.substring(0, this.indexOf(".json"))));
		});
	});
}

function fetchGraph(file){

	var url = 'data/' + file + ".json" + "?val=" + new Date().getTime();;

	$.getJSON( url, function( data ) {
		console.log(data);
		/* Set the global data */
		globalData = data;
		/* Sort by shortest paths first */
		globalData.paths = globalData.paths.sort(function(a,b){
			return a.length > b.length;
		});
		
		/* Populte dropdown with paths */
		$('#pathsList').find('option').remove();
		$("#pathsList").append($("<option />").text("clear"));
		$.each(globalData.paths, function() {
    		$("#pathsList").append($("<option />").text(this[0].substr(0, 12)+" > ("+ this.length +") > "+this[this.length-1].substr(0, 12)));
		});

		/* set the new hash */
		hash = {};
		data.nodes.forEach(function(n){
			hash[n.id] = n;
		})
		/* drawing the graph */
		drawGraph(data);
	});
}

function paint(lst){

	/* resetting all nodes to their original color */
	globalData.nodes.forEach(function(n){
		nodes.update({id:n.id,color:n.color,label:""});
	})

	var n = 1;
	/* painting path */
	lst.forEach(function(nodeId){
		if(nodeId == lst[0] || nodeId == lst[lst.length-1]){
			nodes.update({id:nodeId,label:n});
		}else{
			nodes.update({id:nodeId,label:n, color:"#E57373"});
		}
		n++
	});
}

function destroy() {
  if (network !== null) {
    network.destroy();
    network = null;
  }
}

function drawGraph(data) {

	/* destroy graph first */
	destroy();

	nodes = new vis.DataSet(data.nodes);
	edges = new vis.DataSet(data.edges);

	 var data = {
	    nodes: nodes,
	    edges: edges
	  };

	var options = {
		// you can extend the options like a normal JSON variable:
		layout: {
			hierarchical: {
				//sortMethod: "directed",
				direction: "Down-Up"
			}
		},
		nodes: {
			borderWidth: 2,
			color: {
				border: 'black',
				highlight: {
					border: 'yellow',
					background: 'orange'
				},
			},
			shadow: true,
			physics: true
		},
		edges: {
			arrows: {
				to: {
					enabled: true,
					scaleFactor: 0.5
				},
				middle: {
					enabled: false,
					scaleFactor: 1
				},
				from: {
					enabled: false,
					scaleFactor: 1
				}
			},
			arrowStrikethrough: false,
			dashes: true,
			color: {
				color: '#848484',
				highlight: '#EF5350'
			},
			scaling: {
				min: 1,
				max: 1
			},
			//smooth: false
			smooth: {
				forceDirection: "horizontal",
				type: "diagonalCross",
				roundness: 0.2
			}
		}
	}

    var container = document.getElementById('network');

    // create a network
    network = new vis.Network(container, data, options);

    /* Onclick event */
    network.on("click", function (params) {
		
		$("#name").text(hash[params.nodes[0]].id);
		$("#inchi").text(hash[params.nodes[0]].inchi);
		$("#smiles").text(hash[params.nodes[0]].smiles);
		$("#reaction").text(hash[params.nodes[0]].reaction);

		if(hash[params.nodes[0]].chemSearch == "unknown"){
			$("#jmol_query").val(hash[params.nodes[0]].id);
		}else{
			$("#jmol_query").val(hash[params.nodes[0]].chemSearch);
		}

		$("#jmol_submit").click();
    });

}