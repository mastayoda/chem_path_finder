"use strict";
/**
 * Created by: victor on 03/28/16.
 * Source: isomorphicHelper.js
 * Author: victor
 * Description: Compute the isomorphism coefficeint of two molecules. A set of molecules
                is computed in pasrallel.
 */
var Parallel = require('paralleljs');

function IsomorphicHelper(maxWorkers) {
    this._maxWorkers = maxWorkers;
}

IsomorphicHelper.prototype.computeList = function(lst, dest) {

    /* this object */
    var self = this;

    /* build clean list */
    var paramLst = [];
    /* Dont add suplicates */
    var existent = {};
    lst.forEach(function(mol){
        /* prevent duplicates */
        if(!existent[mol.id]){
            paramLst.push({id: mol.id, inchi: mol.inchi, name: mol.name, smiles:mol.smiles || "", dest: dest});
            existent[mol.id] = true;
        }
    })
    
    /* new parallel instance */
    var p = new Parallel(paramLst, {
        maxWorkers: this._maxWorkers,
    });

    /* return the promise */
    return new Promise(function(resolve, reject) {
        /* compute all isomorphism coefficients */
        p.map(self.computeCoefficient).then(function(results) {
            /* success! return results to promise */
            resolve(results);

        }, function(error){
            /* ups, something went wrong... */
            reject(error);

        });
    });
};

IsomorphicHelper.prototype.computeCoefficient = function(source) {

    var levenshtein = require('fast-levenshtein');

    /* return the levenshtein distance between the two smiles */
    source.coeff = levenshtein.get(source.smiles, source.dest);

    /* return source with coefficient */
    return source;
};

/* export the database */
module.exports = IsomorphicHelper;