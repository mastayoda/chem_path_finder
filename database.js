"use strict";
/**
 * Created by: victor on 12/11/15.
 * Source: database.js
 * Author: victor
 * Description:
 */
var r = require('rethinkdb');

function Database(host, port) {

    /* This object */
    var self = this;
    this.onConnectCallback = null;
    this.conn = null;
    r.connect( {host: host, port: port, db: "reactions"}, function(err, conn) {
        if (err) throw err;
        /* Set the connection */
        self.conn = conn;
        /* if connect callback is present, call */
        if(self.onConnectCallback){
            self.onConnectCallback();
        }
    })
}

Database.prototype.getReactants = function(value, field, fn) {
    
    /* This object */
    var self = this;

    r.table("product")
     .getAll(value,{index: field})
     .eqJoin("id",r.db("reactions").table("reaction_product"),{index:"product_id"})
     .zip()
      .map(function (doc) {
            if(r.row.hasFields('inchi'))
                return doc.merge({product_name: doc('name').default("unknown"),
                            product_inchi: doc('inchi').default("unknown"),
                            product_smiles: doc('smiles').default("unknown")})
             })
    .eqJoin("reaction_id",r.db("reactions").table("reaction_reactant"),{index:"reaction_id"})
     .zip()
     .eqJoin("reactant_id",r.db("reactions").table("reactant"),{index:"primary"})
     .zip()
     .run(self.conn, function(err, cursor) {

        if(err){
            throw err;
        }

        var products = [];
        cursor.toArray(function(err, results) {
            if (err) throw err;
            fn(results);
        });
    });
};

Database.prototype.getReactantsOptimized = function(value, field, fn) {
    
    /* This object */
    var self = this;

    r.table("product")
     .getAll(value,{index: field})
     .eqJoin("id",r.db("reactions").table("reaction_product"),{index:"product_id"})
     .zip()
    .eqJoin("reaction_id",r.db("reactions").table("reaction_reactant"),{index:"reaction_id"})
     .zip()
     .eqJoin("reactant_id",r.db("reactions").table("reactant"),{index:"primary"})
     .zip()
     .run(self.conn, function(err, cursor) {

        if(err){
            throw err;
        }

        var products = [];
        cursor.toArray(function(err, results) {
            if (err) throw err;
            fn(results);
        });
    });
};

/* Receives a reactant and returns all products where that reactant is present */
Database.prototype.getProducts = function(value, field, fn) {

    /* This object */
    var self = this;

    r.table("reactant")
     .getAll(value,{index: field})
     .eqJoin("id",r.db("reactions").table("reaction_reactant"),{index:"reactant_id"})
     .zip()
     .map(function (doc) {
        if(r.row.hasFields('inchi'))
            return doc.merge({reactant_name: doc('name').default("unknown"),
                       reactant_inchi: doc('inchi').default("unknown"),
                         reactant_smiles: doc('smiles').default("unknown")})
         })
     .eqJoin("reaction_id",r.db("reactions").table("reaction_product"),{index:"reaction_id"})
     .zip()
     .eqJoin("product_id",r.db("reactions").table("product"),{index:"primary"})
     .zip()
     .run(self.conn, function(err, cursor) {

        if(err){
            throw err;
        }
        var products = [];
        cursor.toArray(function(err, results) {
            if (err) throw err;
            fn(results);
        });
    });
};

Database.prototype.onConnect = function(fn) {
    /* set the onconnect function */
    this.onConnectCallback = fn;
    /* if is already connected, call */
    if(this.conn){
        this.onConnectCallback();
    }
};

Database.prototype.close = function(fn) {
    this.conn.close({},fn);
};


/* export the database */
module.exports = Database;