 "use strict";

var DB = require("./database.js");
var program = require('commander');
var prettyjson = require('prettyjson');
var Graph = require("graphlib").Graph;
var dot = require("graphlib-dot");
var fs = require('fs');

program
  .version('0.0.1')
  .option('-s, --source <value>', 'The source chemical')
  .option('-t, --target <value>', 'The target chemical')
  .option('-d, --depth <value>', 'The paths depth limits',parseInt)
  .option('-c, --connections <value>', 'The connection limit before halting',parseInt)
  .option('-h, --host <value>', 'The database host')
  .option('-p, --port <value>', 'The database port', parseInt)
  .option('--pathsOnly', 'Only include paths in the resulting graph')
  .option('--shortestPath', 'Only generate the shortest path')
  .option('--edgesInPath', 'Only include edges in path')
  .option('--includePeerReactants', 'Only peer reactants in the reaction(that yields a product)')

  .parse(process.argv);

/* Hashes to store all searches, flags, graph, and connections. */
var hashes = {}, graph = {}, fetched = {}, connections = {}, inPath = {};
/* list of common nodes in the search */
var inCommon = [];
/* Visited Hash for path construction */
var pathSearchVisited = {};
/* Path Strings */
var strPaths = {};
/* By default there are 2 remaining searches, source and target */
var remaining = 2;
/* Not done flag */
var done = false;
/* Hashes for source and destination compounds */
hashes[program.source] = {};
hashes[program.target] = {};
/* fetched hashes flags for source and destination compounds */
fetched[program.source] = {};
fetched[program.target] = {};

/* instantiatig the database */
var db = new DB(program.host, program.port);

/* When database is connects */
db.onConnect(function(){

	/* For the source, get products where this reactant appears, if not as reactant, look into products(it may be a product) */
	db.getProducts(program.source, "name", function(products){

		if(products.length > 0){

			/* Calling the recuersive function */
			searchReactantAsProduct.bind({name:program.source, inchi: products[0].reactant_inchi, smiles: products[0].reactant_smiles, from:program.source, depth: 0, initial: true})(products);
		}else{
			/* Maybe it is as a product...., lets find reactants */
			db.getReactants(program.source, "name", function(reactants){

				/* The object to be binded */
				var bindedObject = {name:program.source, from:program.source, depth: 0, initial: true};
				/* If there is a result, bind the aditional fields(from the searched product) */
				if(reactants.length >0){
					bindedObject.inchi = reactants[0].product_inchi;
					bindedObject.smiles = reactants[0].product_smiles;
				}

				searchReactantAsProduct.bind(bindedObject)(reactants);
			});	
		}
	});

	/* For the target, get reactants for the product. The target is always a product, if not present, halt the execution */
	db.getReactants(program.target, "name", function(reactants){

		/* The object to be binded */
		var bindedObject = {name:program.target, from:program.target, depth: 0, initial: true};
		/* If there is a result, bind the aditional fields(from the searched product) */
		if(reactants.length >0){
			bindedObject.inchi = reactants[0].product_inchi;
			bindedObject.smiles = reactants[0].product_smiles;
		}

		searchReactantAsProduct.bind(bindedObject)(reactants);
	});

});

/* Search reactant as product, get all reactions, and the reactants that yields as product' reactants
 * reactant(self.name) -> products -> reactions -> reactants
*/
function searchReactantAsProduct(reactants){
	
	var self = this;
	/* mark as already fetched, self.from: Initial Product, self.name: Reactant as Product */
	fetched[self.from][self.name] = true;
	/* self.name: Reactant as product, adding a reactant entry in the graph */	
	graph[self.name] = {};
	graph[self.name]["self"] =  self;

	/* self.name: reactant as product, indicate that this reactant as product was found from the source/destination self.from */	
	graph[self.name].from = self.from;
	/* Just a remaining reactant in the queue */
	remaining--;

	/* If the search is done, return */
	if(done){
		return;
	}

	/* if there are reactants */
	if(reactants.length > 0){
		/* Adding reactants to the hash and graph, r is a reactant which belongs to a reaction that yields a product(self.name)*/
		reactants.forEach(function(r){
			/* Mark the origin of the search(from a source/target product)*/
			r.from = self.from;
			/* Add this reactant in the source/destination hash, this will be used to find common reactants in both directoins 
			   of the search. */
			hashes[self.from][r.id] = r;
			/* building graph: graph[reactant as product][reaction].insertIntoArray(reactant) 
			the result is a hash of products, with all of it reactions, with all its reactants.
			for example: 

				graph[ibuprofen] -> [reaction 1 -> [reactant 1, reactant 2, ...], reaction 2 -> [reactant 1, reactant 2, ...]]
			 
			*/
			if(graph[self.name][r.reaction_id]){
				graph[self.name][r.reaction_id].push(r);
			}else{
				graph[self.name][r.reaction_id] = [];
			}
			/* Get the oposite search root: if source, get target, if target, get source */
			var root = (self.from == program.target)?program.source:program.target;
			/* Check if this reactant is already in the oposite search root hash, if so, there is a reactant
			in common! */
			if(hashes[root][r.id]){

				/* mark this reactant as connection */
				r.isConnection  = true;
				r.count = 1;

				/* Add this reactant as a connection, if  already exist, increment the occurence of the connection */
				if(connections[r.name])
					connections[r.name].count++;
				else
					connections[r.name] = r;

				/* If number of desired connections has been found, halt the search */
				if(Object.keys(connections).length >= program.connections && !done){
					/* Set the done flag to stop the remaining asynchronous calls */
					done = true;
					/* find paths in the graph */
					if(program.pathsOnly){
						searchPath();
					}

					if(program.shortestPath){
						purgeToShortestPath();
					}

					/* build the graph dot file */
					buildGraphFile();
				}
			}else{
				/* if is not in the opposite root, this is not a connection between searches */
				r.isConnection  = false;
			}
		});

 		/* Log some progress */
		console.log("Reactants for " + self.name + " " + reactants.length + " from " + self.from + " " + Object.keys(connections).length + " connections found, " + remaining + " remaining, depth: " + self.depth);
		
		/* Recurse with product */
		setTimeout(function(){
			/* For each for this reactants, search them as product and get all reactions with reactants, i.e. recurse */
			reactants.forEach(function(r){
				/* if is not fetched already, and the depth of this root(source/target) is less than the program search depth threshold */
				if(!fetched[self.from].hasOwnProperty(r.name) && self.depth < program.depth){
					/* increment the counter of the pending products to search */
					remaining++;
					/* Mark as fetched to prevent search again */
					fetched[self.from][r.name] = true;
					/* Query database, bind this reactant as product name, from as the search root, increment depth, and mark as not initial search,
					i.e. don't halt the program if not found. */
					db.getReactantsOptimized(r.id, "id", searchReactantAsProduct.bind({name:r.name, inchi:r.inchi, smiles:r.smiles, from: self.from, depth: self.depth+1, initial: false}))
				}
			});
		},1000);

	/* If is initial and there are not results from DB, halt */
	}else if(self.initial){

		console.log(this);
		console.log("ERROR: " + this.name + " is not not present in the database.");
		process.exit();
	}

}

function searchPath(){

	/* Common reactant/products */
	console.log("Searching Paths..");
	searchPathHelper(program.target, {}, []);
}

function searchPathHelper(name, hash, stack){

	/* If visited, return. Else mark as visited, the commented part makes it NP-Complete */
	if(pathSearchVisited[name] && name !== program.source /*hash.hasOwnProperty(name)*/){
		return;
	}else{
		pathSearchVisited[name] = true;
	}

	/* If source reached, add into path */
	if(name == program.source){
		/* The add all elements in the stack to the hash */
		inPath[name] = true;
		stack.forEach(function(p){
			inPath[p] = true;
		});

		var pathlst = stack.slice();
		pathlst.push(name)
		pathlst = pathlst.reverse();

		strPaths[pathlst.join(" -> ")] = pathlst;

		return;
	}

	/* Adding this node to the stack */
	stack.push(name);
	hash[name] = true;

	/* For each reaction in this product*/
	for(var reaction in graph[name]){

		/* if it is the from property, skip */
		if(reaction == "from" || reaction == "self"){
			continue;
		}
		/* For each reactant in this reaction */
		graph[name][reaction].forEach(function(r){
			/* if is not a edge to itself, recurse */
			if(r.name != name){
				searchPathHelper(r.name,hash, stack);
			}
		});
	}
	/* remove this node from the stack */
	stack.pop();
	delete hash[name];
}

function purgeToShortestPath(){


	if(Object.keys(strPaths).length > 0){
		
		console.log("SELECTING SHORTEST OF "+ Object.keys(strPaths).length +" PATHS...");

		var shortestPath;// = strPaths[Object.keys(strPaths)[0]];
		var shortestKey;//= Object.keys(strPaths)[0];
		var shortestLen = Number.MAX_SAFE_INTEGER;
		var newInPath = [];

		/* Looking for the shortest path */
		for(var p in strPaths){

			//if(strPaths[p].length < shortestLen && !missingInchi){
			if(strPaths[p].length <= shortestLen){
				shortestPath = strPaths[p];
				shortestKey = p;
				shortestLen = shortestPath.length;
			}
		}

		/* If there is a viable shortest path available */
		if(shortestPath){
			/* Purging nodes not in the shortest path */
			shortestPath.forEach(function(product){
				/* if this compound is not in the shortest path, remove */
				if(inPath[product]){
					newInPath[product] = true;
				}
			})

			/* Cleaning and setting only the shortest patsh */
			strPaths = {};
			strPaths[shortestKey] = shortestPath;
			/* Set the new hash */
			inPath = newInPath;
		}
	}
}

/* This will build the graph as a dot file */
function buildGraphFile(){

	console.log(Object.keys(inPath).length + " NODES IN PATH...");

	/* hash for already added nodes */
	var isPresent = {};
	/* Edge construction hash */
	var edgeHash = {};
	/* Product construction hash */
	var addedHash = {};
	/* Mandatory edges in path */
	var allowedEdges = {};
	/* Graph initialization */
	var g = {};
	g.nodes = [];
	g.edges = [];
	g.paths = [];

	var sourcePresent = false;
	var targetPresent = false;

	/* Add all paths */
	for(var p in strPaths){
		g.paths.push(strPaths[p]);
		/* Building allowed edges */
		for(var i = 1; i < strPaths[p].length; i++){
			allowedEdges[strPaths[p][i-1] + "-" +strPaths[p][i]] = true;
		}
	}

	/* build the graph */
	for(var product in graph){	

		/* If we only want products in the path, and this product is not in the path, skip */
		if(program.pathsOnly && typeof inPath[product] === 'undefined'){
			continue;
		}

		/* Check if we should add this product node */
		if(!isPresent[product]){
			
			if(product == program.source){
				sourcePresent = true;
				g.nodes.push({id: product,color:"#A7FFEB", chemSearch: product, inchi: graph[product].self.inchi, smiles: graph[product].self.smiles});
			}else if(product == program.target){
				targetPresent = true;
				g.nodes.push({id: product,color:"#A7FFEB", chemSearch: product, inchi:graph[product].self.inchi, smiles: graph[product].self.smiles});
			}else{
				g.nodes.push({id: product,color:"#F5F5F5", chemSearch: product, inchi:graph[product].self.inchi, smiles: graph[product].self.smiles});
			}

			/* mark as present */
			isPresent[product] = true;
		}

		for(var reaction in graph[product]){
			/* add each product */
			if(reaction !== "from" && reaction !== "self"){
				graph[product][reaction].forEach(function(p){

					/* If we only want products in the path, and this product is not in the path, skip */
					if(program.pathsOnly && typeof inPath[p.name] === 'undefined'){
						return;
					}

					/* Saving product and reaction id */
					addedHash[product] = reaction;

					/* Adding this reactant */
					addPeerReactant(g, product, p, isPresent, sourcePresent, targetPresent,reaction);

					/* Add the reactant node -> reaction node */
					if(p.name !== product && !edgeHash.hasOwnProperty(p.name+"-"+product)){

						/* If we only want edges in the path */
						if(program.edgesInPath){
							/* Lets check if this edge is allowed */
							if(allowedEdges.hasOwnProperty(p.name+"-"+product)){
								g.edges.push({from: p.name, to: product, reaction: reaction});
								edgeHash[p.name+"-"+product]=true;
							}
						}else{
							g.edges.push({from: p.name, to: product, reaction: reaction});
							edgeHash[p.name+"-"+product]=true;
						}
					}
				});
			}
		}

	}

	/* if include peer reactants */
	if(program.includePeerReactants)
	{
		console.log("INCLUDING PEER REACTANTS");
		
		/* For all edges */
		g.edges.forEach(function(edge){

			/* Get the reactants of the destination with the specific reaction id */
			var reactants = graph[edge.to][edge.reaction];

			reactants.forEach(function(p){
				/* Adding this node */
				addPeerReactant(g, edge.to, p, isPresent, sourcePresent, targetPresent,edge.reaction);
				/* Adding this edge */
				g.edges.push({from: p.name , to: edge.to, reaction: edge.reaction});
			})
			
		})
	}

	/* If there are paths, write the file */
	if(Object.keys(strPaths).length > 0){

		fs.writeFileSync("./webViz/data/" + program.target.replace(" ","_") + "_" + 
			program.source.replace(" ","_")  + "_conn_" + 
			Object.keys(connections).length + 
			"_depth_"+program.depth +
			".json",
			JSON.stringify(g));

	}else if(!sourcePresent){
		console.log(program.source + " NOT REACHABLE!");
	}else {
		console.log(program.target + " NOT REACHABLE!");
	}

	/* Display all paths */
	console.log(Object.keys(strPaths));

	db.close(function(){
		console.log("Connection closed...");
		process.exit();
	});
}

function addPeerReactant(g, product, p, isPresent, sourcePresent, targetPresent, reaction){

	/* Check if we can fix missing inchi and smiles */
	if(graph[p.name] && p.smiles == "unknown" && graph[p.name].self.smiles !== "unknown"){
		p.smiles = graph[p.name].self.smiles;
		p.id = p.smiles;
	}
	if(graph[p.name] && p.inchi == "unknown" && graph[p.name].self.inchi !== "unknown"){
		p.inchi = graph[p.name].self.inchi;
		p.id = p.inchi;
	}

	/* Check if we should add this reaction node */
	if(!isPresent[p.name]){
		/* add node to  the reaction */
		if(p.name == program.source){
			sourcePresent = true;
			g.nodes.push({id: p.name,color:"#A7FFEB", chemSearch: p.id, inchi:p.inchi, smiles: p.smiles, reaction: reaction });
		}else if(p.name == program.target){
			targetPresent = true;
			g.nodes.push({id: p.name,color:"#A7FFEB", chemSearch: p.id, inchi:p.inchi, smiles: p.smiles, reaction: reaction });
		}
		else if (p.isConnection){
			g.nodes.push({id: p.name,color:"#F4FF81", chemSearch: p.id, inchi:p.inchi, smiles: p.smiles, reaction: reaction});
		}else{
			g.nodes.push({id: p.name,color:"#F5F5F5", chemSearch: p.id, inchi:p.inchi, smiles: p.smiles, reaction: reaction});
		}
		/* mark as present */
		isPresent[p.name] = true;
	}
}

/* If Ctrl + C is pressed, halt the program */
process.on('SIGINT', function(){
	
	done = true;
	/* find paths in the graph */
	if(program.pathsOnly){
		searchPath();
	}

	if(program.shortestPath){
		purgeToShortestPath();
	}

	/* build the graph dot file */
	buildGraphFile();

	db.close(function(){
		console.log("Connection closed...");
	});

	process.exit();
});
